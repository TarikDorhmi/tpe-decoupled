// Include gulp.
var gulp 			= require('gulp');
var browsersync 	= require('browser-sync').create();
var config 			= require('./config.json');

var sass 			= require('gulp-sass');
var glob 			= require('gulp-sass-glob');
var plumber 		= require('gulp-plumber');
var notify 			= require('gulp-notify');
var sourcemaps 		= require('gulp-sourcemaps');
var jshint 			= require('gulp-jshint');
var concat 			= require('gulp-concat');

// CSS.
gulp.task('css', function (cb) {
	gulp.src(config.css.src)
		.pipe(glob())
		.pipe(plumber({
			errorHandler: function (error) {
				notify.onError({
					title: "Gulp",
					subtitle: "Failure!",
					message: "Error: <%= error.message %>",
					sound: "Beep"
				})(error);
				this.emit('end');
			}
		}))
		.pipe(sourcemaps.init())
		.pipe(sass({
			style: 'expanded',
			errLogToConsole: true,
			includePaths: config.css.includePaths
		}))
		.pipe(sourcemaps.write('./'))
		.pipe(gulp.dest(config.css.dest))
		.pipe(browsersync.stream());

	cb();
});


// JavaScript.
gulp.task('js', function (cb) {
	gulp.src(config.js.src, {allowEmpty: true})
		.pipe(jshint())
		.pipe(jshint.reporter('default'))
		.pipe(sourcemaps.init())
		.pipe(concat(config.js.file, {newLine: ';'}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.js.dest));

	cb();
});


// Bundle Javascript libraries
gulp.task('js:libs', function (cb) {
	gulp.src(config.js['libs'])
		.pipe(sourcemaps.init())
		.pipe(concat(config.js['libs:filename'], {newLine: ';\n\n'}))
		.pipe(sourcemaps.write())
		.pipe(gulp.dest(config.js.dest));

	cb()
});


gulp.task('reload', function (cb) {
	browsersync.reload();

	cb();
});


gulp.task('watch', function () {
	gulp.watch(config.css.src, gulp.task('css'));
	gulp.watch(config.js.src, gulp.task('js'));
	gulp.watch(config.tpl.src, gulp.task('reload'));
});


gulp.task('serve', function () {
	browsersync.init({
		proxy: config.proxy,
		open: false,
		reloadOnRestart: true,
		ghostMode: false,
		port: 3000
	});
});


gulp.task('default', gulp.parallel('serve', 'watch'));
