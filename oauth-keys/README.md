# Installation

## Generate a pair of keys to encrypt the tokens.

```
openssl genrsa -out private.key 2048
openssl rsa -in private.key -pubout > public.key
```

Save the path to your keys in: /admin/config/people/simple_oauth.
