![alternativetext](./logo.png)

VOID Drupal 8 Headless Factory.

# Install

```
$ git clone git@bitbucket.org:adminvoid/vactory_decoupled.git
$ cd vactory_decoupled
$ export COMPOSER_MEMORY_LIMIT=-1
$ composer install
```

# Deploy

Configure `capistrano/config/deploy/staging.rb` file

```
$ cd capistrano
$ bundle install
$ cap staging deploy
```

### TODO:
- Documentation

### NOTES - Deploy
```
mkdir -p /home/vactory3/shared/web/sites/default
ln -s /home/vactory3/backend/web /home/vactory3/public_html/backend
```

Create file `/home/vactory3/shared/web/sites/default/settings.local.php` with the following content:

```
<?php

# Database settings.
$databases['default']['default'] = [
  'database' => 'databasename',
  'username' => 'sqlusername',
  'password' => 'sqlpassword',
  'host' => '127.0.0.1',
  'port' => '3306',
  'driver' => 'mysql',
  'prefix' => '',
  'collation' => 'utf8mb4_general_ci',
];
```
